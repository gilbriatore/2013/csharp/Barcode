//Author Serdar Dirican (serdardirican@hotmail.com)
//Thanks for Andreas Schmidt (a_j_schmidt@rocketmail.com)

using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using BarcodeCreator;

namespace BarcodeCreator
{
	public enum PrintPreview{Preview,Print};
	public class Form1 : System.Windows.Forms.Form
	{
		PrintDocument doc = new PrintDocument();
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.ComboBox comboBox1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.FontDialog fontDialog1;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.ComboBox comboBox2;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.ComboBox comboBox3;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.TextBox textBox3;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private BarcodeCreator.AsBarcode asBarcode1;
		public System.Windows.Forms.ContextMenu contextMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.Button PrintButton;
		private System.Windows.Forms.Button PreviewButton;
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.PreviewButton = new System.Windows.Forms.Button();
			this.PrintButton = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.comboBox3 = new System.Windows.Forms.ComboBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.comboBox2 = new System.Windows.Forms.ComboBox();
			this.button1 = new System.Windows.Forms.Button();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.fontDialog1 = new System.Windows.Forms.FontDialog();
			this.asBarcode1 = new BarcodeCreator.AsBarcode();
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pictureBox1
			// 
			this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
			this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
			this.pictureBox1.Location = new System.Drawing.Point(0, 0);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(486, 145);
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.PreviewButton);
			this.panel1.Controls.Add(this.PrintButton);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.textBox3);
			this.panel1.Controls.Add(this.checkBox1);
			this.panel1.Controls.Add(this.comboBox3);
			this.panel1.Controls.Add(this.textBox2);
			this.panel1.Controls.Add(this.comboBox2);
			this.panel1.Controls.Add(this.button1);
			this.panel1.Controls.Add(this.comboBox1);
			this.panel1.Controls.Add(this.textBox1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(0, 145);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(486, 115);
			this.panel1.TabIndex = 1;
			// 
			// PreviewButton
			// 
			this.PreviewButton.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.PreviewButton.Location = new System.Drawing.Point(408, 88);
			this.PreviewButton.Name = "PreviewButton";
			this.PreviewButton.Size = new System.Drawing.Size(56, 23);
			this.PreviewButton.TabIndex = 14;
			this.PreviewButton.Text = "Preview";
			this.PreviewButton.Click += new System.EventHandler(this.preview_Click);
			// 
			// PrintButton
			// 
			this.PrintButton.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.PrintButton.Location = new System.Drawing.Point(336, 88);
			this.PrintButton.Name = "PrintButton";
			this.PrintButton.Size = new System.Drawing.Size(56, 23);
			this.PrintButton.TabIndex = 13;
			this.PrintButton.Text = "Print";
			this.PrintButton.Click += new System.EventHandler(this.PrintButton_Click);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(160, 88);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(64, 16);
			this.label3.TabIndex = 12;
			this.label3.Text = "Width";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(160, 48);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(64, 16);
			this.label2.TabIndex = 11;
			this.label2.Text = "Angle";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(160, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 10;
			this.label1.Text = "Enter text";
			// 
			// textBox3
			// 
			this.textBox3.Location = new System.Drawing.Point(232, 88);
			this.textBox3.Name = "textBox3";
			this.textBox3.ReadOnly = true;
			this.textBox3.Size = new System.Drawing.Size(72, 20);
			this.textBox3.TabIndex = 9;
			this.textBox3.Text = "";
			// 
			// checkBox1
			// 
			this.checkBox1.Location = new System.Drawing.Point(232, 64);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.TabIndex = 7;
			this.checkBox1.Text = "Check sum";
			this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
			// 
			// comboBox3
			// 
			this.comboBox3.Items.AddRange(new object[] {
														   "None",
														   "Code",
														   "Typ",
														   "Both"});
			this.comboBox3.Location = new System.Drawing.Point(296, 40);
			this.comboBox3.Name = "comboBox3";
			this.comboBox3.Size = new System.Drawing.Size(80, 21);
			this.comboBox3.TabIndex = 5;
			this.comboBox3.Text = "None";
			this.comboBox3.TextChanged += new System.EventHandler(this.comboBox3_TextChanged);
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(232, 40);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(56, 20);
			this.textBox2.TabIndex = 4;
			this.textBox2.Text = "0";
			this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
			// 
			// comboBox2
			// 
			this.comboBox2.Items.AddRange(new object[] {
														   "Code_2_5_interleaved",
														   "Code_2_5_industrial",
														   "Code_2_5_matrix",
														   "Code39",
														   "Code39Extended",
														   "Code128A",
														   "Code128B",
														   "Code128C",
														   "Code93",
														   "Code93Extended",
														   "CodeMSI",
														   "CodePostNet",
														   "CodeCodabar",
														   "CodeEAN8",
														   "CodeEAN13",
														   "CodeUPC_A",
														   "CodeUPC_E0",
														   "CodeUPC_E1",
														   "CodeUPC_Supp2",
														   "CodeUPC_Supp5",
														   "CodeEAN128A",
														   "CodeEAN128B",
														   "CodeEAN128C"});
			this.comboBox2.Location = new System.Drawing.Point(344, 8);
			this.comboBox2.Name = "comboBox2";
			this.comboBox2.Size = new System.Drawing.Size(121, 21);
			this.comboBox2.TabIndex = 3;
			this.comboBox2.Text = "CodePostNet";
			this.comboBox2.TextChanged += new System.EventHandler(this.comboBox2_TextChanged);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(16, 80);
			this.button1.Name = "button1";
			this.button1.TabIndex = 1;
			this.button1.Text = "Font";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// comboBox1
			// 
			this.comboBox1.DisplayMember = "0";
			this.comboBox1.Items.AddRange(new object[] {
														   "TopLeft",
														   "TopRight",
														   "TopCenter",
														   "BottomLeft",
														   "BottomRight",
														   "BottomCenter"});
			this.comboBox1.Location = new System.Drawing.Point(16, 56);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(91, 21);
			this.comboBox1.TabIndex = 0;
			this.comboBox1.Text = "TopLeft";
			this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(232, 8);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(105, 20);
			this.textBox1.TabIndex = 2;
			this.textBox1.Text = "";
			this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
			// 
			// asBarcode1
			// 
			this.asBarcode1.Angle = 0;
			this.asBarcode1.Color = System.Drawing.Color.White;
			this.asBarcode1.ColorBar = System.Drawing.Color.Black;
			this.asBarcode1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.asBarcode1.Height = 50;
			this.asBarcode1.Left = 30;
			this.asBarcode1.Modul = 2;
			this.asBarcode1.Ratio = 2;
			this.asBarcode1.ShowTextFont = new System.Drawing.Font("Arial", 8.25F);
			this.asBarcode1.ShowTextPosition = BarcodeCreator.ShowTextPosition.TopLeft;
			this.asBarcode1.Text = null;
			this.asBarcode1.Top = 50;
			this.asBarcode1.Typ = BarcodeCreator.BarcodeType.CodePostNet;
			this.asBarcode1.Width = 0;
			this.asBarcode1.Change += new System.EventHandler(this.asBarcode1_Change);
			// 
			// contextMenu1
			// 
			this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						 this.menuItem1,
																						 this.menuItem2,
																						 this.menuItem3});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.Shortcut = System.Windows.Forms.Shortcut.F1;
			this.menuItem1.Text = "Help";
			this.menuItem1.Click += new System.EventHandler(this.Menu_Click);
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 1;
			this.menuItem2.Text = "About";
			// 
			// menuItem3
			// 
			this.menuItem3.Index = 2;
			this.menuItem3.Text = "Print";
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(486, 260);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.pictureBox1);
			this.Name = "Form1";
			this.Text = "BarcodeCreator";
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void asBarcode1_Change(object sender, System.EventArgs e)
		{
			Graphics gr=pictureBox1.CreateGraphics();
			gr.Clear(Color.White);
			asBarcode1.DrawBarcode(gr);
			textBox3.Text=asBarcode1.Width.ToString();
		}

		private void textBox1_TextChanged(object sender, System.EventArgs e)
		{
			asBarcode1.Text=textBox1.Text;
		}

		private void comboBox2_TextChanged(object sender, System.EventArgs e)
		{
			asBarcode1.Typ=(BarcodeType)comboBox2.SelectedIndex;
		}

		private void textBox2_TextChanged(object sender, System.EventArgs e)
		{
			try
			{
				asBarcode1.Angle=Convert.ToDouble(textBox2.Text);
			}
			catch
			{
				asBarcode1.Angle=0;
			}
		}

		private void comboBox3_TextChanged(object sender, System.EventArgs e)
		{
			asBarcode1.ShowText=(BarcodeOption)comboBox3.SelectedIndex;
		}

		private void checkBox1_CheckedChanged(object sender, System.EventArgs e)
		{
			asBarcode1.CheckSum=checkBox1.Checked;
		}

		private void pictureBox1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			asBarcode1.Top=e.Y;
			asBarcode1.Left=e.X;			
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			fontDialog1.Font=asBarcode1.ShowTextFont;
			if(fontDialog1.ShowDialog()==DialogResult.OK)
			{
				asBarcode1.ShowTextFont=fontDialog1.Font;
			}
		}

		private void comboBox1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			asBarcode1.ShowTextPosition=(ShowTextPosition)comboBox1.SelectedIndex;
		}

		private void Menu_Click(object sender, System.EventArgs e)
		{
			Menu.GetForm();
		}
		private void PrintButton_Click(object sender, System.EventArgs e)
		{
			Print(PrintPreview.Print);
		}
		private void preview_Click(object sender, System.EventArgs e)
		{
			Print(PrintPreview.Preview);
		}
		private void Print(PrintPreview PVOption)
		{   
			doc.PrintPage += new PrintPageEventHandler(this.asBarcode1.doc_PrintPage);
		
			if(PVOption == PrintPreview.Preview)
			{
				PrintPreviewDialog pd = new PrintPreviewDialog();
				pd.Document = doc;
				pd.ShowDialog();
				pd.Dispose();
			}
			else
				doc.Print();
		}
		//This is going to need data, if not graphics or
		//whatever else initially was sent in here.
		//string data,Graphics gr
		
	}
}
